Add-Type -AssemblyName System.speech
$Narrator = New-Object System.Speech.Synthesis.SpeechSynthesizer
$Narrator.SelectVoice('Microsoft Zira Desktop')
$Location = ".\output.txt"
$Contents = Get-Content $Location
$Narrator.Speak($Contents)