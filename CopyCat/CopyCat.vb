﻿Imports System.IO

Public Class CopyCat

    Dim Offset As Integer
    Dim Point1 As Point
    Dim Point2 As Point
    Dim Dragging As Boolean = False
    Dim pictureBox1 As New PictureBox

    Private Sub SnippingTool_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        FormBorderStyle = FormBorderStyle.None
        WindowState = FormWindowState.Maximized
        Me.KeyPreview = True
        Offset = 5
    End Sub





    Private Sub SnippingTool_MouseDown(sender As Object, e As MouseEventArgs) Handles MyBase.MouseDown
        If (e.Button.Equals(MouseButtons.Left) = False) Then Return
        Point1 = e.Location
        Dragging = True
        CaptureWindow.Location = e.Location
        CaptureWindow.Show()
    End Sub

    Private Sub SnippingTool_MouseMove(sender As Object, e As MouseEventArgs) Handles MyBase.MouseMove
        If Dragging Then
            Point2 = e.Location
            Dim X0 As Integer = Point1.X
            Dim Y0 As Integer = Point1.Y
            Dim X1 As Integer = Point2.X
            Dim Y1 As Integer = Point2.Y

            If X0 = X1 OrElse Y0 = Y1 Then Exit Sub

            Dim width = Math.Abs(X1 - X0)
            Dim height = Math.Abs(Y1 - Y0)
            Dim captureSize As Size = New Size(width, height)
            Dim origin As Point = New Point(Math.Min(X0, X1) - Offset, Math.Min(Y0, Y1) - Offset)
            CaptureWindow.Size = New System.Drawing.Size(captureSize)
            CaptureWindow.Location = origin
        End If
    End Sub

    Private Sub SnippingTool_MouseUp(sender As Object, e As MouseEventArgs) Handles MyBase.MouseUp

        Point2 = e.Location
        Dragging = False
        CaptureWindow.Size = New System.Drawing.Size(0, 0)
        CaptureWindow.Hide()
        Dim X0 As Integer = Point1.X
        Dim Y0 As Integer = Point1.Y
        Dim X1 As Integer = Point2.X
        Dim Y1 As Integer = Point2.Y

        If X0 = X1 OrElse Y0 = Y1 Then Exit Sub

        Dim width = Math.Abs(X1 - X0)
        Dim height = Math.Abs(Y1 - Y0)
        Dim captureSize As Size = New Size(width, height)
        Dim origin As Point = New Point(Math.Min(X0, X1) - Offset, Math.Min(Y0, Y1) - Offset)

        Dim destination = New Rectangle(0, 0, width, height)
        Dim capture As Bitmap = New Bitmap(width, height)
        Dim G As Graphics = Graphics.FromImage(capture)
        G.CopyFromScreen(origin, New Point(0, 0), captureSize)

        capture.Save("capture.jpg", Drawing.Imaging.ImageFormat.Jpeg)

        Dim shell
        shell = CreateObject("wscript.shell")
        shell.run("run-tesseract-invisibly.bat")
        shell = Nothing
        WindowState = FormWindowState.Minimized
        Application.Exit()

    End Sub

    Private Sub SnippingTool_KeyPress(sender As Object, e As KeyPressEventArgs) Handles MyBase.KeyPress

        Application.Exit()

    End Sub
End Class
